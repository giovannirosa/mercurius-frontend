import { CustomerTableComponent } from './components/customer-table/customer-table.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CustomerFormComponent } from './components/customer-form/customer-form.component';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/security/login/login.component';
import { ModuleWithProviders } from '@angular/core';
import { AuthGuard } from './components/security/auth.guard';
import { AgendaComponent } from './components/agenda/agenda.component';

export const ROUTES: Routes = [
  { path: 'login' , component: LoginComponent },
  { path: '' , component:  DashboardComponent, canActivate: [AuthGuard]},
  //{ path: 'clientes-form' , component: CustomerFormComponent, canActivate: [AuthGuard] },
  { path: 'clientes' , component: CustomerTableComponent, canActivate: [AuthGuard] },
  { path: 'agenda' , component: AgendaComponent, canActivate: [AuthGuard] }
];

export const routes: ModuleWithProviders = RouterModule.forRoot(ROUTES);

