import {Telephone} from './telephone';

export class Customer {
    constructor(
        public id: string,
        public name: string,
        public reputation: string,
        public gender: string,
        public telephone: Array<string>,
        public email: Array<string>,
        public birthday: Date,
        public cpf: string,
        public period: number,
        public notes: string,
        public dateEntered: Date
    ) {}
}
