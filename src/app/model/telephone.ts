export class Telephone {
  constructor(
    public type: string,
    public number: string,
    public notes: string,
    public dateEntered: Date
  ) {}


}
