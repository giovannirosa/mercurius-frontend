import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { ResponseApi } from '../../model/response-api';
import { CustomerService } from '../../services/customer/customer.service';
import { SharedService } from '../../services/shared.service';
import { Customer } from '../../model/customer';
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { NgForm, AbstractControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

@Component({
  selector: 'app-customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {

  @ViewChild('form')
  form: NgForm;
  shared: SharedService;
  customer: Customer;
  original: Customer;
  message: {};
  classCss: {};

  constructor(
    private customerService: CustomerService,
    public dialogRef: MatDialogRef<CustomerFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog) {
      this.shared = SharedService.getInstance();
      this.customer = data.customer;
      const telCopy = [];
      let i = 0;
      this.customer.telephone.forEach(element => {
        telCopy[i++] = element;
      });
      const emailCopy = [];
      i = 0;
      this.customer.email.forEach(element => {
        emailCopy[i++] = element;
      });
      this.original = new Customer(this.customer.id, this.customer.name, this.customer.reputation,
        this.customer.gender, telCopy, emailCopy, this.customer.birthday,
        this.customer.cpf, this.customer.period, this.customer.notes, this.customer.dateEntered);
      dialogRef.disableClose = true;
  }

  ngOnInit() {
    // this.dialogRef.updateSize('80%', '80%');
  }

  close() {
    // console.log(JSON.stringify(this.original));
    // console.log(JSON.stringify(this.customer));
    if (JSON.stringify(this.original) === JSON.stringify(this.customer)) {
        this.dialogRef.close();
    } else {
      const confirmDialog = this.dialog.open(ConfirmationDialogComponent);
      confirmDialog.componentInstance.message = 'Existem alterações que não foram salvas. Deseja mesmo fechar?';
      confirmDialog.afterClosed().subscribe(result => {
        console.log(result);
        if (result) {
          this.dialogRef.close();
        }
      });
    }
  }

  findById(id: string): void {
    this.customerService.findById(id).subscribe((responseApi: ResponseApi) => {
      this.customer = responseApi.data;
  } , err => {
    this.showMessage({
      type: 'error',
      text: err['error']['errors'][0]
    });
  });
  }

  register() {
    this.message = {};
    if (this.customer.name === '' ||
      this.customer.reputation === '' ||
      this.customer.gender === '' ||
      this.customer.telephone[0] === '') {
      return;
    }
    this.customerService.createOrUpdate(this.customer).subscribe((responseApi: ResponseApi) => {
        this.customer = this.shared.newCustomer();
        const customer: Customer = responseApi.data;
        this.form.resetForm();
        this.dialogRef.close();
        this.showMessage({
          type: 'success',
          text: `Registered ${customer.name} successfully`
        });
    } , err => {
      this.showMessage({
        type: 'error',
        text: err['error']['errors'][0]
      });
    });
  }

  getFormGroupClass(model: AbstractControl): {} {
    return {
      'form-group': true,
      'has-error' : !model.valid && (model.dirty),
      'has-success' : model.valid && (model.dirty)
    };
  }

  private showMessage(message: {type: string, text: string}): void {
      this.message = message;
      this.buildClasses(message.type);
      setTimeout(() => {
        this.message = undefined;
      }, 3000);
  }

  private buildClasses(type: string): void {
     this.classCss = {
       'alert': true
     };
     this.classCss['alert-' + type] =  true;
  }
}
