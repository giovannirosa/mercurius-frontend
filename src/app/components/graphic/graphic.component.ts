import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-graphic',
  templateUrl: './graphic.component.html',
  styleUrls: ['./graphic.component.css']
})
export class GraphicComponent implements OnInit {

  chart;

  constructor() { }

  ngOnInit() {
    this.chart = new Chart('areaChart', {
      type: 'line',
      data: {
        labels  : ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho'],
        datasets: [
          {
            label                     : 'Entrada',
            backgroundColor           : 'rgba(63,226,38,0.9)',
            borderColor               : 'rgba(63,226,38,1)',
            pointBackgroundColor      : 'rgba(63,226,38,1)',
            pointBorderColor          : 'rgba(63,226,38,1)',
            pointHoverBackgroundColor : '#fff',
            pointHoverBorderColor     : 'rgba(63,226,38,1)',
            data                      : [65, 59, 80, 81, 56, 55, 40]
          },
          {
            label                     : 'Saída',
            backgroundColor           : 'rgba(236,23,23,0.9)',
            borderColor               : 'rgba(236,23,23,1)',
            pointBackgroundColor      : 'rgba(236,23,23,1)',
            pointBorderColor          : 'rgba(236,23,23,1)',
            pointHoverBackgroundColor : '#fff',
            pointHoverBorderColor     : 'rgba(236,23,23,1)',
            data                      : [28, 48, 40, 19, 86, 27, 90]
          }
        ]
      },
      options: {
        maintainAspectRatio: false,
        legend: {
          display: false
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true
          }]
        },
        elements: {
          point: {
              radius: 0,
              hitRadius: 10, 
              hoverRadius: 10
          }
        }
      }
    });
  }

}
