import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { ResponseApi } from '../../model/response-api';
import { CustomerService } from '../../services/customer/customer.service';
import { SharedService } from '../../services/shared.service';
import {Component, ViewChild, Input, OnInit} from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { Customer } from 'src/app/model/customer';
import { CustomerFormComponent } from '../customer-form/customer-form.component';

/**
 * @title Table with filtering
 */
@Component({
  selector: 'app-customer-table',
  styleUrls: ['customer-table.component.css'],
  templateUrl: 'customer-table.component.html',
})
export class CustomerTableComponent implements OnInit {
  displayedColumns: string[] = ['name', 'reputation', 'mobile', 'actions'];
  sizeOpt: number[] = [5, 10, 20];
  @Input() simplified = false;
  customer: Customer;
  message: {};
  classCss: {};
  shared: SharedService;
  dataSource = new MatTableDataSource<Customer>();
  filterFieldWidth = '50%';

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private customerService: CustomerService,
              public dialog: MatDialog) {
    this.shared = SharedService.getInstance();
    this.customer = this.shared.newCustomer();
  }

  ngOnInit() {
    if (this.simplified) {
      this.displayedColumns = ['name', 'reputation', 'mobile'];
      this.sizeOpt = [1, 3, 5];
      this.filterFieldWidth = '100%';
    }
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.filterPredicate = function(data, filter: string): boolean {
      return data.name.toLowerCase().includes(filter) ||
            data.reputation.toLowerCase().includes(filter) ||
            data.telephone[0].toString().includes(filter);
    };
    this.findAll();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  findAll() {
    this.customerService.findAll().subscribe((responseApi: ResponseApi) => {
      this.dataSource.data = responseApi.data;
    } , err => {
      console.log(err['error']['errors'][0]);
    });
  }

  findById(id: string): void {
    this.customerService.findById(id).subscribe((responseApi: ResponseApi) => {
      this.customer = responseApi.data;
    } , err => {
      console.log(err['error']['errors'][0]);
    });
  }

  create(): void {
    const dialogRef = this.dialog.open(CustomerFormComponent, {
      data: {
        customer: this.shared.newCustomer(),
        close: 'Cancelar',
        ok: 'Criar'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.findAll();
    });
  }

  details(customer: Customer) {
    const dialogRef = this.dialog.open(CustomerFormComponent, {
      data: {
        customer: customer,
        close: 'Fechar',
        ok: 'Salvar'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.findAll();
    });
  }

  delete(customer: Customer): void {
    // console.log(customer);
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);
    dialogRef.componentInstance.message = `Deletar ${customer.name} permanentemente?`;
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result) {
        this.customerService.delete(customer.id).subscribe((responseApi: ResponseApi) => {
          this.findAll();
        });
      }
    });
  }

  // private showMessage(message: {type: string, text: string}): void {
  //   this.message = message;
  //   this.buildClasses(message.type);
  //   setTimeout(() => {
  //     this.message = undefined;
  //   }, 5000);
  // }

  // private buildClasses(type: string): void {
  //   this.classCss = {
  //     'alert': true
  //   };
  //   this.classCss['alert-' + type] =  true;
  // }
}
