import { SharedService } from './../../services/shared.service';
import {MediaMatcher} from '@angular/cdk/layout';
import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import { UserInfoComponent } from '../user-info/user-info.component';
import { MatDialog } from '@angular/material';

/** @title Responsive sidenav */
@Component({
  selector: 'app-navigation',
  templateUrl: 'navigation.component.html',
  styleUrls: ['navigation.component.css'],
})
export class NavigationComponent implements OnDestroy, OnInit {
  mobileQuery: MediaQueryList;
  showTemplate = false;
  public shared: SharedService;

  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef,
              media: MediaMatcher,
              public dialog: MatDialog) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.shared = SharedService.getInstance();
  }

  ngOnInit() {
    this.shared.showTemplate.subscribe(
      show => {
        console.log('showTemplate', show);
        return this.showTemplate = show;
      }
    );
    this.showTemplate = this.shared.user !== null;
    console.log('showTemplate', this.showTemplate);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  openUserInfo(): void {
    const dialogRef = this.dialog.open(UserInfoComponent);
  }
}
