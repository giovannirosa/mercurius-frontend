import { Component, OnInit, EventEmitter } from '@angular/core';
import { SharedService } from 'src/app/services/shared.service';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {

  shared: SharedService;

  constructor(private router: Router,
              public dialogRef: MatDialogRef<UserInfoComponent>) {
    this.shared = SharedService.getInstance();
   }

  ngOnInit() {
  }

  logOut() {
    this.dialogRef.close();
    this.shared.logOut();
    this.router.navigate(['/login']);
  }

  close() {
    this.dialogRef.close();
  }

}
