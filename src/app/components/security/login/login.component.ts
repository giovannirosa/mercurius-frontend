import { CurrentUser } from '../../../model/currentUser';
import { SharedService } from '../../../services/shared.service';
import { UserService } from '../../../services/user/user.service';
import { User } from '../../../model/user';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatIconRegistry, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = new User('', '', '', '');
  shared: SharedService;
  hide = true;
  @ViewChild('form')
  form: NgForm;

  constructor(private userService: UserService,
              private router: Router,
              iconRegistry: MatIconRegistry,
              sanitizer: DomSanitizer,
              private snackBar: MatSnackBar) {
    this.shared = SharedService.getInstance();
    iconRegistry.addSvgIcon(
      'tati',
      sanitizer.bypassSecurityTrustResourceUrl('/src/assets/img/hair-cut.svg'));
  }

  ngOnInit() {
  }


  login() {
    // console.log("ip");
    // this.userService.getIpAddress().subscribe(data => {
    //   console.log(data);
    // });
    this.userService.login(this.user).subscribe((userAuthentication: CurrentUser) => {
        this.shared.logIn(userAuthentication);
        this.router.navigate(['/']);
        console.log('Login');
    } , err => {
      this.shared.logOut();
      console.log('Erro');
      this.snackBar.open('Erro de conexão!', 'Fechar');
    });
  }



  cancelLogin() {
    this.user = new User('', '', '', '');
    this.form.resetForm();
    // window.location.href = '/login';
    // window.location.reload();
  }

  getFormGroupClass(isInvalid: boolean, isDirty: boolean): {} {
    return {
      'form-group': true,
      'has-error' : isInvalid  && isDirty,
      'has-success' : !isInvalid  && isDirty
    };
  }

}
