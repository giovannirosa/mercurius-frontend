import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customer'
})
export class CustomerPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (args === 'cpf') {
      return value.substring(0, 3) + '.' + value.substring(3, 6) +
        '.' + value.substring(6, 9) + '-' + value.substring(9, 11);
    } else if (args === 'tel') {
      return '(' + value.substring(0, 2) + ') ' +
        value.substring(2, 7) + '-' + value.substring(7, 11);
    } else if (args === 'rep') {
      if (value === 'Good') {
        return 'Boa';
      } else if (value === 'Attention') {
        return 'Atenção';
      } else if (value === 'Bad') {
        return 'Ruim';
      }
    }
    // this.cpf = this.cpf.substring(0, 3) + '.' + this.cpf.substring(3, 6) +
    //   '.' + this.cpf.substring(6, 9) + '-' + this.cpf.substring(9, 11);
    // this.telephone = '(' + this.telephone.substring(0, 2) + ')' +
    //   this.telephone.substring(2, 7) + '-' + this.telephone.substring(7, 11);


    return value;
  }

}
