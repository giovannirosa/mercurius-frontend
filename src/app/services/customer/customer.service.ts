import { Customer } from '../../model/customer';
import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { HELP_DESK_API } from '../mercurius.api';

@Injectable()
export class CustomerService {

  constructor(private http: HttpClient) {}

  createOrUpdate(customer: Customer) {
    if (customer.id !== null && customer.id !== '') {
      return this.http.put(`${HELP_DESK_API}/api/customer`, customer);
    } else {
      customer.id = null;
      return this.http.post(`${HELP_DESK_API}/api/customer`, customer);
    }
  }

  findAll() {
    return this.http.get(`${HELP_DESK_API}/api/customer`);
  }

  findById(id: string) {
    return this.http.get(`${HELP_DESK_API}/api/customer/${id}`);
  }

  delete(id: string) {
    return this.http.delete(`${HELP_DESK_API}/api/customer/${id}`);
  }

}
