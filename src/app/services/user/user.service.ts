import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from './../../model/user';
import { Injectable } from '@angular/core';
import { HELP_DESK_API } from '../mercurius.api';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {}

  login(user: User) {
    return this.http.post(`${HELP_DESK_API}/api/auth`, user);
  }

  createOrUpdate(user: User) {
    if (user.id != null && user.id !== '') {
      return this.http.put(`${HELP_DESK_API}/api/user`, user);
    } else {
      user.id = null;
      return this.http.post(`${HELP_DESK_API}/api/user`, user);
    }
  }

  findAll(page: number, count: number) {
    return this.http.get(`${HELP_DESK_API}/api/user/${page}/${count}`);
  }

  findById(id: string) {
    return this.http.get(`${HELP_DESK_API}/api/user/${id}`);
  }

  delete(id: string) {
    return this.http.delete(`${HELP_DESK_API}/api/user/${id}`);
  }

   // Get IP Adress using http://freegeoip.net/json/?callback
  getIpAddress() {
    return this.http
          .get('http://freegeoip.net/json/?callback')
          .pipe(map(response => response || {}));
  }

  private handleError(error: HttpErrorResponse):
    Observable<any> {
      // Log error in the browser console
      console.error('observable error: ', error);

      return Observable.throw(error);
  }
}
