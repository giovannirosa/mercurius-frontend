import { Observable } from 'rxjs';
import { Injectable, EventEmitter, Output } from '@angular/core';
import { User } from '../model/user';
import {Customer} from '../model/customer';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { CurrentUser } from '../model/currentUser';

@Injectable()
export class SharedService {

  public static instance: SharedService = null;
  user: User;
  token: string;
  showTemplate = new EventEmitter<boolean>();

  constructor() {
    console.log('SharedService [constructor]');
    this.user = JSON.parse(sessionStorage.getItem('user'));
    this.token = sessionStorage.getItem('token');
    this.showTemplate.emit(true);
    return SharedService.instance = SharedService.instance || this;
  }

  public static getInstance() {
    if (this.instance == null) {
      this.instance = new SharedService();
    }
    return this.instance;
  }

  newCustomer(): Customer {
    return new Customer('', '', '', '', new Array<string>(), new Array<string>(), null, '', 0, '', new Date());
  }

  isLoggedIn(): boolean {
    // console.log(this.token);
    if (this.user == null) {
      return false;
    }
    return this.user.email !== '';
  }

  logIn(userAuthentication: CurrentUser) {
    sessionStorage.setItem('token', userAuthentication.token);
    sessionStorage.setItem('user', JSON.stringify(userAuthentication.user));
    this.token = userAuthentication.token;
    this.user = userAuthentication.user;
    this.user.profile = this.user.profile.substring(5);
    this.showTemplate.emit(true);
  }

  logOut() {
    sessionStorage.removeItem('user');
    sessionStorage.removeItem('token');
    this.user = null;
    this.token = null;
    this.showTemplate.emit(false);
  }

}
