import { CustomerPipe } from './pipes/customer.pipe';
import { CustomerTableComponent } from './components/customer-table/customer-table.component';
import { AuthInterceptor } from './components/security/auth.interceptor';
import { SharedService } from './services/shared.service';
import { AuthGuard } from './components/security/auth.guard';
import { UserService } from './services/user/user.service';
import { LoginComponent } from './components/security/login/login.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './components/navigation/navigation.component';
import { FlexLayoutModule } from '@angular/flex-layout';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { CustomerFormComponent } from './components/customer-form/customer-form.component';
import {MaterialModule} from './material';
import { GraphicComponent } from './components/graphic/graphic.component';
import { routes } from './app.routes';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CustomerService } from './services/customer/customer.service';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS, MAT_SNACK_BAR_DEFAULT_OPTIONS } from '@angular/material';
import {registerLocaleData} from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
// import { ResponsiveColsDirective } from './directives/responsive-cols.directive';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AgendaComponent } from './components/agenda/agenda.component';
import {
  CalendarDateFormatter,
  CalendarModule,
  CalendarMomentDateFormatter,
  DateAdapter as CalendarDateAdapter,
  MOMENT
} from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/moment';
import * as moment from 'moment';
import { UserInfoComponent } from './components/user-info/user-info.component';

registerLocaleData(localePt, 'pt-BR');

export function momentAdapterFactory() {
  return adapterFactory(moment);
}

const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY-MM-DD',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'YYYY-MM-DD',
    monthYearA11yLabel: 'MMMM YYYY'
  },
};

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    DashboardComponent,
    CustomerFormComponent,
    GraphicComponent,
    LoginComponent,
    CustomerTableComponent,
    CustomerPipe,
    ConfirmationDialogComponent,
    // ResponsiveColsDirective,
    AgendaComponent,
    UserInfoComponent
  ],
  entryComponents: [
    ConfirmationDialogComponent,
    CustomerFormComponent,
    UserInfoComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    routes,
    HttpClientModule,
    FormsModule,
    FlexLayoutModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    CalendarModule.forRoot({
      provide: CalendarDateAdapter,
      useFactory: momentAdapterFactory
    },
    {
      dateFormatter: {
        provide: CalendarDateFormatter,
        useClass: CalendarMomentDateFormatter
      }
    })
  ],
  providers: [
    UserService,
    AuthGuard,
    SharedService,
    CustomerService,
    { provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    { provide: LOCALE_ID,
      useValue: 'pt-BR'
    },
    { provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    { provide: MAT_DATE_FORMATS,
      useValue: MY_FORMATS
    },
    { provide: MOMENT,
      useValue: moment
    },
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
      useValue: {duration: 5000}
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
