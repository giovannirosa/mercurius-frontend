import {NgModule} from '@angular/core';
import {
  MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatCheckboxModule,
  MatGridListModule, MatCardModule, MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule,
  MatSelectModule, MatInputModule, MatDatepickerModule, MatSliderModule,
  MatStepperModule, MatTooltipModule, MatTabsModule, MatSnackBarModule, MatSlideToggleModule, MatRippleModule,
  MatRadioModule, MatProgressSpinnerModule, MatProgressBarModule, MatExpansionModule, MatDialogModule,
  MatChipsModule, MatButtonToggleModule, MatAutocompleteModule
} from '@angular/material';
import {MatMomentDateModule} from '@angular/material-moment-adapter';

const modules = [
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule,
  MatMomentDateModule,
];

@NgModule({
  imports: [...modules],
  exports: [...modules],
})

export class MaterialModule { }
