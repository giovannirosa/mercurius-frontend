import { CurrentUser } from './model/currentUser';
import { Component, OnInit } from '@angular/core';
import { SharedService } from './services/shared.service';
import { UserService } from './services/user/user.service';
import { User } from './model/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  showTemplate = false;
  public shared: SharedService;

  constructor(private userService: UserService) {
    this.shared = SharedService.getInstance();
    // const now = moment();
    // console.log('hello world', now.format()); // add this 3 of 4
    // console.log(now.add(7, 'days').format()); // add this 4of 4
  }

  ngOnInit() {
    this.shared.showTemplate.subscribe(
      show => this.showTemplate = show
    );
  //   this.shared.user = new User('','admin@mercurius.com','123456','');
  //   // this.userService.login(this.shared.user);
  //   this.userService.login(this.shared.user).subscribe((userAuthentication: CurrentUser) => {
  //     this.shared.token = userAuthentication.token;
  //     this.shared.user = userAuthentication.user;
  //     this.shared.user.profile = this.shared.user.profile.substring(5);
  //     // this.shared.showTemplate.emit(true);
  //     // this.router.navigate(['/']);
  // } , err => {
  //   this.shared.token = null;
  //   this.shared.user = null;
  //   // this.shared.showTemplate.emit(false);
  //   // this.message = 'Erro ';
  // });
  }

  showContentWrapper() {
    return {
      'content-wrapper': this.shared.isLoggedIn()
    };
  }
}
